### Nutrition
- Food rule: 100 cal per 10 miles minimally
- Water rule: 21 oz per 25 miles minimally 

### Platform 
If you are looking for a platform to provide suggestions for your health needs, check out https://cronometer.com/

## Suggestions
Fluids:
- https://nuunlife.com/products/nuun-sport

Gel
- https://guenergy.com/products/energy-gel?variant=31169146814515
- https://honeystinger.com/collections/all-energy-chews_2/products/pomegranate-passionfruit-energy-chews

Solids
- https://shop.clifbar.com/products/clif-bar-blueberry-almond-crisp