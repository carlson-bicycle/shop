### Clothing
When picking clothing for riding a bicycle during fall and winter seasons, the goal is to focus on your core to extremities. 

Here are a few options that I tend to use.

### Temp 60&deg; F+
![60 Degree Temp](https://www.dropbox.com/scl/fi/2a5d2qhmcncdaffq61ndz/60.jpg?rlkey=01dvdlihqqufsy6wk1ds79aog&raw=1)
- bike shorts and jersey

### Temp 50&deg; F+ 
![50 Degree Temp](https://www.dropbox.com/scl/fi/o7p4i0ep45ktoe39fi4f9/50.jpg?rlkey=85er9n8sng8snlprgu1h0z6hu&raw=1)
- jacket or long sleeve 

### Temp 40&deg;F+
![40 Degree Temp](https://www.dropbox.com/scl/fi/03exhi3j9u7fis7bajkdp/40.jpg?rlkey=pggxwdz8uht0fx2dkdcnl8b5x&raw=1)
- thermal layer (optional)
- long top and bottom

### Temp 30&deg; F+
![30 Degree Temp](https://www.dropbox.com/scl/fi/lnnnowka9l108y7ofhz4s/30.jpg?rlkey=ygsipfquhyrj1bvqjtorhueiw&raw=1)
- thermal layer (optional)
- long top and bottom
- soft shell top and bottom

### Temp 20&deg;F+
- thermal layer
- long top and bottom
- soft shell top and bottom