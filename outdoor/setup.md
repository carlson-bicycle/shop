### Recommended Setup
- Shoes: https://www.trekbikes.com/us/en_US/bike-clothing/cycling-shoes/road-bike-shoes/bontrager-solstice-road-cycling-shoe/p/33678/?colorCode=black
- Pedal: https://ride.shimano.com/collections/pedals-spd/products/pd-m520
- Bike: https://www.trekbikes.com/us/en_US/bikes/road-bikes/performance-road-bikes/domane/domane-al/domane-al-2-gen-3/p/33083/?colorCode=red_black
- Tire: https://www.continental-tires.com/products/b2c/bicycle/tires/terra-speed-protection/
- Phone Mount: https://www.quadlockcase.com/collections/shop-cycle/products/bike-mount?variant=179963062
