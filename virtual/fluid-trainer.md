### Fluid Trainer Setup

## Requirements
- [Fluid Trainer](https://www.performancebike.com/saris-fluid-2-trainer-fluid-resistance-9904t/p1008035)
- [Quick Release Trainer Skewer](https://www.performancebike.com/saris-quick-release-trainer-skewer-silver-9714t/p1008044)
- [Leveling Block](https://www.performancebike.com/saris-leveling-block-9702t/p1008040)
- [Wahoo Sensors](https://a.co/d/e5QJ1kc) 
- [Training Tire](https://a.co/d/5SQ3Uu2)
- [iPad Air](https://www.apple.com/ipad-air/)
- [Zwift Subscription ($15)](https://www.zwift.com/create-account?origin=shopify&redirect_uri=https%3A%2F%2Feu.zwift.com%2Ffree-trial)

## Optional
- [Fan](https://www.homedepot.com/pep/Lasko-20-in-3-Speeds-Box-Fan-in-White-with-Save-Smart-Technology-for-Energy-Efficiency-Carry-Handle-B20201/203072133?source=shoppingads&locale=en-US&pla&mtc=SHOPPING-CM-CML-GGL-D29A-029_017_ACS_FANS-NA-Multi-NA-PMAX-4038230-NA-NA-NA-NBR-NA-NA-NA-MinorAppl&cm_mmc=SHOPPING-CM-CML-GGL-D29A-029_017_ACS_FANS-NA-Multi-NA-PMAX-4038230-NA-NA-NA-NBR-NA-NA-NA-MinorAppl-71700000111902223--&gclid=Cj0KCQjwjt-oBhDKARIsABVRB0yPv_bRbuU29uk9MdF0sHVIlTFe8VULxlFAduCkWNLAZF7cV04Ra6caAh4zEALw_wcB&gclsrc=aw.ds)
- [Heart Rate Monitor](https://a.co/d/7OcFZef)
- [Sweat Bands](https://a.co/d/0l2i2WJ)
- [Sweat Net](https://a.co/d/0jr11lF)