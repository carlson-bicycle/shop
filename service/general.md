### General Service

## Daily Care
- Align and adjust brakes
- Tune and adjust shifting
- Torque all fasteners

## Weekly Care
- Deep clean drivetrain
- Lateral and vertical wheel true
- Inspect bottom bracket
- Clean and polish bike

## Annual Care
- Disassemble and deep clean all parts
- New cables, housing, and bearing systems*
- Advanced wheel true
- Replace front and rear suspension wipers, seals, and oil
