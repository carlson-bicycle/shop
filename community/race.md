### Races 

2024 Rides:
- Mid South in Stillwater OK
- Robidoux Rendezvous in Gering NE
- [Grounded Gravel](https://www.bikereg.com/grounded-nebraska-gravel-festival) in Roca NE
- [Big Sugar](https://www.bigsugarclassic.com/gravel/) in Bentonville AR
- Rule of 3 in Bentonville AR
- Ned Gravel in Nederland CO
- Rad Dirt Fest in Trinidad CO
- Ragbrai across Iowa
- Wyo 131 in Lander WY

Other IOWA Rides:
- https://www.bikereg.com/events/Gravel-Grinder/IOWA/