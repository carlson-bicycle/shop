### Southwood Cycling Club
Welcome to Southwood Cycling Club! 

Nice to meet you'll virtually and thank you for being interested in the club. The intent of this club is to be all inclusive for those wanting to cycle with others in the community. Exercise has been a large part of my life and I look forward to sharing my passion with you.<br/>

I grew up in a small town south of Kearney, Nebraska and have run competitively since 6th grade (since about 2000). Due to a running injury in College at Doane University, I switched from running to cycling. If you have watched the Lincoln Marathon or Good Life Halfsy, I have been one of the cycle assists since 2011; when I moved to Lincoln. I have also been a seasonal running coach in Lincoln.<br/>

Details can be found at [Southwood Lutheran](https://southwoodlutheran.church/faith-growth/group-finder/group-detail/223855/)

# Technology
- [Zwift](https://www.zwift.com/clubs/f14b4851-bc2f-4551-bf89-fbdef5550f52/home)
- [Discord](https://discord.gg/zk3QxTT2QR)
- [Strava](https://www.strava.com/clubs/1106216)
- [Facebook](https://www.facebook.com/groups/684881366446372)